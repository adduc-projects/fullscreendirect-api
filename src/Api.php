<?php

namespace Adduc\FullscreenDirect;

class Api
{
    use ApiRequest;

    private $login_domain = "https://stagebloc.com";
    protected $client_id;
    protected $client_secret;
    protected $request_uri;
    protected $access_token;

    /**
     * @param string $client_id
     * @param string $client_secret
     * @param string $request_uri
     */
    public function __construct($client_id, $client_secret, $request_uri)
    {
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->request_uri = $request_uri;
    }

    /**
     * @return string
     */
    public function getLoginUrl()
    {
        $data = array(
            'client_id' => $this->client_id,
            'consumer_key' => $this->client_id,
            'redirect_uri' => $this->request_uri,
            'response_type' => "code",
            'scope' => "non-expiring"
        );

        return $this->login_domain . "/connect?" . http_build_query($data);
    }

    /**
     * @param Authorization\Authorization $authorization
     * @return bool
     */
    public function login(Authorization\Authorization $authorization)
    {
        $this->access_token = $authorization->getAccessToken(
            $this->client_id,
            $this->client_secret,
            $this->request_uri
        );

        return !!$this->access_token;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->access_token;
    }

    /**
     * @return Entity\User
     */
    public function getMyUser()
    {
        $user = $this->get('users/me');
        return new Entity\User($user);
    }

    /**
     * @param int $user_id
     * @return Entity\User
     */
    public function getUser($user_id)
    {
        $user = $this->get('users/' . $user_id);
        return new Entity\User($user);
    }

    /**
     * @return Entity\Account[]
     */
    public function getMyAccounts()
    {
        $accounts = $this->get('accounts');
        foreach ($accounts as $key => $account) {
            $accounts[$key] = new Entity\Account($account);
        }
        return $accounts;
    }

    /**
     * @param int $account_id
     * @return Entity\Account
     */
    public function getAccount($account_id)
    {
        $account = $this->get('account/' . $account_id);
        return new Entity\Account($account);
    }

    /**
     * @return Entity\FanClub[]
     */
    public function getMyFanClubs()
    {
        $fan_clubs = $this->get('account/fanclubs/following');
        foreach ($fan_clubs as $key => $fan_club) {
            $fan_clubs[$key] = new Entity\FanClub($fan_club);
        }
        return $fan_clubs;
    }

    /**
     * @param int $account_id
     * @return Entity\FanClub
     */
    public function getFanClub($account_id)
    {
        $fan_club = $this->get('account/' . $account_id . '/fanclub');
        return new Entity\FanClub($fan_club);
    }
}
