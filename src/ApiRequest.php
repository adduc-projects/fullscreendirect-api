<?php

namespace Adduc\FullscreenDirect;

use GuzzleHttp\Client;

trait ApiRequest
{
    /** @property string */
    private $api_url = "https://api.stagebloc.com/v1/";

    /**
     * @param string $url
     * @param array $data
     * @return array
     */
    protected function post($url, array $data = array())
    {
        $client = new Client(['base_uri' => $this->api_url]);

        $response = $client->request('POST', $url, [
            'form_params' => $data
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        if (empty($data['metadata']['http_code']) || $data['metadata']['http_code'] != 200) {
            throw new \Exception(print_r($data, true));
        }

        return $data['data'];
    }

    /**
     * @param string $url
     * @param array $data
     * @return array
     */
    protected function get($url, array $data = array())
    {
        $client = new Client(['base_uri' => $this->api_url]);

        $headers = ['Authorization' => 'OAuth ' . $this->access_token];

        $response = $client->request('GET', $url, [
            'query' => $data,
            'headers' => $headers
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        if (empty($data['metadata']['http_code']) || $data['metadata']['http_code'] != 200) {
            throw new \Exception(print_r($data, true));
        }

        return $data['data'];
    }
}
