<?php

namespace Adduc\FullscreenDirect\Entity;

abstract class Entity
{
    /**
     * @param array $data
     */
    public function __construct(array $data = null)
    {
        if (!$data) {
            return;
        }
        
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            } else {
                trigger_error(get_called_class() . "::\${$key} doesn't exist.");
            }
        }
    }
}
