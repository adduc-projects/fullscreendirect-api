<?php

namespace Adduc\FullscreenDirect\Entity;

class Tier extends Entity
{
    public $title;
    public $description;
    public $price;
    public $discount;
    public $membership_length_interval;
    public $membership_length_unit;
    public $renewal_price;
    public $can_submit_content;
}
