<?php

namespace Adduc\FullscreenDirect\Entity;

class Address extends Entity
{
    public $id;
    public $name;
    public $street_address;
    public $street_address_2;
    public $city;
    public $state;
    public $postal_code;
    public $country;
}
