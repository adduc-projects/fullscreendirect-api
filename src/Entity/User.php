<?php

namespace Adduc\FullscreenDirect\Entity;

class User extends Entity
{
    public $id;
    public $url;
    public $created;
    public $name;
    public $username;
    public $bio;
    public $color;
    public $birthday;
    public $gender;
    public $email;
    public $phone_number;

    /** @property Address */
    public $address;

    /** @property Photo */
    public $photo;

    /**
     * @param array $data
     */
    public function __construct(array $data = null)
    {
        parent::__construct($data);
        $this->address = new Address($this->address);
        $this->photo = new Photo($this->photo);
    }
}
