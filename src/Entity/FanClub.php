<?php

namespace Adduc\FullscreenDirect\Entity;

class FanClub extends Entity
{
    public $title;
    public $description;
    public $account;
    public $moderation_queue;
    public $tier_info;
    public $allowed_content_sections;
    public $user_tier;
    public $user_membership;

    /**
     * @param array $data
     */
    public function __construct(array $data = null)
    {
        parent::__construct($data);
        if ($this->tier_info) {
            foreach ($this->tier_info as $key => $tier) {
                $this->tier_info[$key] = new Tier($tier);
            }
        }
        $this->user_membership = new UserMembership($this->user_membership);
    }
}
