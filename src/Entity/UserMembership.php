<?php

namespace Adduc\FullscreenDirect\Entity;

use DateTime;

class UserMembership extends Entity
{
    public $tier;
    public $tier_plan;
    public $trial;
    public $expiration;
    public $created;
    public $next_payment_date;
    public $auto_renew;

    /**
     * @param array $data
     */
    public function __construct(array $data = null)
    {
        parent::__construct($data);
        $this->created = new DateTime($this->created);
        $this->expiration = new DateTime($this->expiration);
        $this->tier_plan = new TierPlan($this->tier_plan);
    }
}
