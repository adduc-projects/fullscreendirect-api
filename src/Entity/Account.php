<?php

namespace Adduc\FullscreenDirect\Entity;

class Account extends Entity
{
    public $id;
    public $url;
    public $stagebloc_url;
    public $name;
    public $description;
    public $type;
    public $stripe_enabled;
    public $color;
    public $verified;
    public $photo;
    public $user_is_admin;
    public $user_is_following;
    public $user_role;
    public $custom_domain;
}
