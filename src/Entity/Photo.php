<?php

namespace Adduc\FullscreenDirect\Entity;

use DateTime;

class Photo extends Entity
{
    public $id;
    public $account;
    public $title;
    public $category;
    public $created;
    public $modified;
    public $short_url;
    public $description;
    public $width;
    public $height;
    public $sticky;
    public $exclusive;
    public $in_moderation;
    public $is_fan_content;
    public $comment_count;
    public $like_count;
    public $images;
    public $user;
    public $custom_field_data;
    public $user_has_liked;

    /**
     * @param array $data
     */
    public function __construct(array $data = null)
    {
        parent::__construct($data);
        $this->created = new DateTime($this->created);
        $this->modified = new DateTime($this->modified);
    }
}
