<?php

namespace Adduc\FullscreenDirect\Entity;

class TierPlan extends Entity
{
    public $id;
    public $price;
    public $period_type;
    public $period_unit;
}
