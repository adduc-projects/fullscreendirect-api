<?php

namespace Adduc\FullscreenDirect\Authorization;

use Adduc\FullscreenDirect\ApiRequest;

class Credential implements Authorization
{
    use ApiRequest;

    /**
     * @property string
     */
    protected $username;

    /**
     * @property string
     */
    protected $password;

    /**
     * @property string
     */
    protected $access_token;

    /**
     * @param string $username
     * @param string $password
     */
    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @param string $client_id
     * @param string $client_secret
     * @param string $redirect_uri
     * @return string
     */
    public function getAccessToken($client_id, $client_secret, $redirect_uri)
    {
        if (!isset($this->access_token)) {
            $data = $this->post('oauth2', array(
                'client_id' => $client_id,
                'redirect_uri' => $redirect_uri,
                'username' => $this->username,
                'password' => $this->password,
                'response_type' => 'code'
            ));

            if ($data['code']) {
                $request_token = new RequestToken($data['code']);
                $this->access_token = $request_token->getAccessToken(
                    $client_id,
                    $client_secret,
                    $redirect_uri
                );
            }
        }

        return $this->access_token;
    }
}
