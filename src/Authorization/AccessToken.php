<?php

namespace Adduc\FullscreenDirect\Authorization;

class AccessToken implements Authorization
{
    /**
     * @property string
     */
    protected $access_token;

    /**
     * @param string
     */
    public function __construct($access_token)
    {
        $this->access_token = $access_token;
    }

    /**
     * @param string $client_id
     * @param string $client_secret
     * @param string $redirect_uri
     * @return string
     */
    public function getAccessToken($client_id, $client_secret, $redirect_uri)
    {
        return $this->access_token;
    }
}
