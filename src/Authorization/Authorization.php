<?php

namespace Adduc\FullscreenDirect\Authorization;

use Api;

interface Authorization
{
    /**
     * @param string $client_id
     * @param string $client_secret
     * @param string $redirect_uri
     * @return string
     */
    public function getAccessToken($client_id, $client_secret, $redirect_uri);
}
