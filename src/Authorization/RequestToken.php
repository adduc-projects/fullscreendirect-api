<?php

namespace Adduc\FullscreenDirect\Authorization;

use Adduc\FullscreenDirect\ApiRequest;

class RequestToken implements Authorization
{
    use ApiRequest;

    /**
     * @property string
     */
    protected $request_token;

    /**
     * @param string $request_token
     */
    public function __construct($request_token)
    {
        $this->request_token = $request_token;
    }

    /**
     * @param string $client_id
     * @param string $client_secret
     * @param string $redirect_uri
     * @return string
     */
    public function getAccessToken($client_id, $client_secret, $redirect_uri)
    {
        if (!isset($this->access_token)) {
            $data = $this->post('oauth2/token', array(
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'grant_type' => 'authorization_code',
                'code' => $this->request_token
            ));

            if (!empty($data['access_token'])) {
                $this->access_token = $data['access_token'];
            }
        }

        return $this->access_token;
    }
}
