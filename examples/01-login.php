<?php

use Adduc\FullscreenDirect\Api;
use Adduc\FullscreenDirect\Authorization\Credential;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/config.php';

$api = new Api($config['id'], $config['secret'], $config['request_uri']);

$credential = new Credential($config['username'], $config['password']);

if (!$api->login($credential)) {
    echo "Can't login";
    exit;
}

echo "Logged in! <br>\n";
echo "Access Token: {$api->getAccessToken()}";
