<?php

use Adduc\FullscreenDirect\Api;
use Adduc\FullscreenDirect\Authorization\AccessToken;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/config.php';

$api = new Api($config['id'], $config['secret'], $config['request_uri']);

$credential = new AccessToken($config['access_token']);
$api->login($credential);

$accounts = $api->getMyAccounts();

var_dump($accounts);
