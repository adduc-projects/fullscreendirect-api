Fullscreen Direct API PHP Wrapper
===

Introduction
---

A wrapper for the Fullscreen Direct (formerly known as StageBloc) API written
in PHP.

Getting Started
---

To be able to test your application, you'll need a Fullscreen Direct account.
After you've secured an account, you'll need to [register your application]
and receive a client ID and secret.


<!-- Links -->
[register your application]: http://stagebloc.com/account/admin/management/developers/
